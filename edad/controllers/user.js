//controllers/user.js
module.exports = function(app){

    var User = require('../models/user');

    //Create a new user and save it
    user = function(req, res){
        var user = new User({name: req.body.name, age: req.body.age});
        user.save();
        res.end();
    };

    //find all people
    list = function(req, res){
        User.find(function(err, users) {
            res.send(users);
        });
    };

    //find person by id
    find = (function(req, res) {
        User.findOne({_id: req.params.id}, function(error, user) {
            res.send(user);
        })
    });

    //isMayor
    isMayor = function(req, res){
    };

    //Link routes and functions
    app.post('/user', user);
    app.get('/users', list);
    app.get('/user/:id', find);
}