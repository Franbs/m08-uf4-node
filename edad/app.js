var express = require('express'), mongoose = require('mongoose'), http = require('http'), bodyParser = require('body-parser');
var app = express();
var port = 3000;
var views = __dirname + "/views";

mongoose.connect("mongodb://localhost:27017/edad");

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.use("/", (req, res) => {
    res.sendFile(views + "/index.html");
});

app.post("/mayor", (req, res) => {
    var myData = new User(req.body);
    myData.save()
        .then(item => {
            res.send("item saved to database");
        })
        .catch(err => {
            res.status(400).send("unable to save to database");
        });
});

app.listen(port, () => {
    console.log("Server listening on port " + port);
});