var express = require('express'),
    mongoose = require('mongoose'),
    http = require('http'),
    app = express();

//app.use(express.json());
app.set('view engine', 'html');
app.use(express.urlencoded({ extended: true }));
//app.use(express.static(path));

const UserRouter = require('./controllers/User');

mongoose.connect('mongodb+srv://admin:admin@cluster0.upami.mongodb.net/myFirstDatabase?retryWrites=true&w=majority',
    {
        useNewUrlParser: true,
        useUnifiedTopology: true
    }).then(() => {
        console.log("DB Connected");
    }).catch((err) => console.log(err));

app.use('/user', UserRouter);

app.listen(3000, function () {
    console.log('Example app listening on port https://localhost:3000');
});