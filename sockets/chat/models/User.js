const mongoose = require("mongoose");
 
// create an schema
var userSchema = new mongoose.Schema({
    name: String,
    password: String
});
 
module.exports = mongoose.model('User', userSchema);