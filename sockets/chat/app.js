var express = require('express'), 
    mongoose = require('mongoose'),
    bodyParser = require('body-parser'),
    app = express(),
    port = 3000,
    views = __dirname + "/views",
    User = require("./models/User");

app.set("view engine", "html");
app.use(bodyParser.urlencoded({ extended: true }));

app.use("/", (req, res) => {
    res.sendFile(views + "/index.html");
});
    
app.listen(port, () => {
    console.log("Server listening on port https://localhost:" + port);
});    