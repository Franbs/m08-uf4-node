// routes/index.js

//Definimos la ruta donde encontrar los controladores usando la libreria Path
var path = require("path");
var ctrlDir = path.resolve("controllers/");

//Importamos el controllador
var userCtrl = require(path.join(ctrlDir, "user"));
var router = express.Router();

//Link routes and functions

  app.post('/user', userCtrl.add);
  app.get('/users', userCtrl.list);
  app.get('/user/:id', userCtrl.find);